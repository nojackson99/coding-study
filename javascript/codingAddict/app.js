// Vartiable lookup

const globalNumber = 5;

function add(num1, num2){
    //if this dosent exist js will use 5 for statment in line 10 instead of 3
    const globalNumber = 3;

    //js will look for globalNumber in local scope then if not found will look in global scope
    const result = num1 + num2 + globalNumber;

    function multiply(){
        //js will look in this scope then scope of add before checking global
        const multiplyResult = result * globalNumber;
        console.log(multiplyResult);
    }

    multiply();
    return result;
}

console.log(add(3,4));