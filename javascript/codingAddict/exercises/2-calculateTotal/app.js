//Calculate Total exercise

//arrays to track purchases on gas and food
const gasPurchases = [43, 33, 25, 44, 11];
const foodPurcahses = [12, 9, 15, 14, 21, 62];

//calculates total of elements in array
function calculateTotal(arr) {
    total = 0;

    //iterates through passed in array and stores in total in const total
    for (let index = 0; index < arr.length; index++) {
        console.log(arr[index]);
        total += arr[index];
    }

    if(total > 150) {
        console.log(`you have spent too much!!!!`)
    }
    else{
        console.log(`your spending is okay!`)
    }
    

    return total;
}

//sets total for gas and food purchases to variables
const gasTotal = calculateTotal(gasPurchases);
const foodTotal = calculateTotal(foodPurcahses);

//logs food and gas totals to console
console.log(`Money spent on gas: $${gasTotal}`);
console.log(`Money spent on food: $${foodTotal}`);

//method for console logging a coder defined object for debugging
console.log( {
    gasTotal: gasTotal,
    foodTotal: foodTotal,
    var: 22
})