//Arrays, Functions, and Objects
//Arrays - [], 0 index based

const friends = ["Alyssa", "Andrew", "Josh", "Connor", "Katelyn", "Ricky", "Eishat"];
console.log(friends);

personAndrew = 'Andrew';
personAlyssa = 'Alyssa';
personJosh = 'Josh';

//function declaration
function hello() {
    console.log('Hello there Alyssa');
    console.log('Hello there Andrew');
    console.log('Hello there Josh');
}

function greetPerson(person) {              //person here is a function parameter
    console.log('Hello there ' + person)
}

//function call
hello();

//Greet people one at a time
greetPerson(personAlyssa);                 //personAlyssa here is a function argument
greetPerson(personAndrew);
greetPerson(personJosh);
