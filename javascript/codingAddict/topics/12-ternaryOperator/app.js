//ternary operator

//executes code by checking variable to see if it is true or false

const value = 1 < 0;
const value2 = 1 > 0;

//uses var ? <if_true_code>:<if_false_code> syntax
value ? console.log(`value is true`):console.log(`value is false`);
value2 ? console.log(`value is true`):console.log(`value is false`);