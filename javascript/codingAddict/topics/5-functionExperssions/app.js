//Function expressions
//Assigning a function to a created variable

//function declaration
function addValues(num1, num2) {
    return num1 + num2;
}

const firstValue = addValues(3, 4);
const secondValue = addValues(3, 4);

//funciton expression
//here we are creating a funciton expression with the addValues function declared above
const add = function (num1, num2) {     //when creating function expression a name is not needed for the function
    return num1 + num2;                 //this is then called an annonymous function
}                                       //this may cause bugs until I learn about hoisting

const values = [firstValue, secondValue];
console.log(values);