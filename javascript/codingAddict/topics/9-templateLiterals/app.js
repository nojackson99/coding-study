// template literals

//${} is called interpolation and allows for inserting expressions
const nameVar = 'john';
const age = 25;

const sentence = "Hey it's " + nameVar + ' and he is ' + age + ' years old';
console.log(sentence);

//replaces string concatonation with single or double quote strings
//uses backtics ``
const value = `Hey it's ${nameVar} and he is ${age} years old.`
console.log(value);

console.log(`simple math using backticks and interplolation. 4 + 4 is: ${4 + 4}`);



