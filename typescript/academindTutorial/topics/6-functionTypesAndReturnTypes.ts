//Function types:
//functions have types just like variables
//types are inferred by typescript or can be defined by the coder
//type for add function is number and type for printResult function is void because printResult is not returning anything

function add(n1: number, n2: number)
{
    return n1 + n2;
}

//void function used when function does not return anything
//undefined is also a type but is not used often
function printResult(num: number) {
    console.log('Result: ' + num);
}

printResult(add(5, 12));

//let combineValues: Function; setting type to function tells typescript that only functions can be stored in this variable
                            // This can still result in error if combine values is set to the wrong function ex: printResult

//This is solved by setting a specific funciton type
let combineValues: (a: number, b: number) => number; //this means combineValues can store functions that have two parameters
                                                     //with the number type and have number as return type
    

//setting value of comineValues to be funciton add
combineValues = add;

//this allows for executing add by calling combineValues and passing in expected parameters
//unsure of why this is better than console.log(add());
console.log(combineValues(8, 8));
console.log(add(8, 8));

/*
if combineValues was not given a function type this code would cause error
combineValues = 8;
console.log(add(8,8)) ***ERROR*** because combineValues data is no logner a function this would cause an error ***ERROR***
*/