function add(n1: number, n2: number)
{
    return n1 + n2;
}

function printResult(num: number) {
    console.log('Result: ' + num);
}

function addAndHandle(n1: number, n2: number, cb: (num: number) => void) { //cb is a callback function
    const result = n1 + n2;
    cb(result);
}

printResult(add(5, 12));

let combineValues: (a: number, b: number) => number;
    
combineValues = add;

console.log(combineValues(8, 8));
console.log(add(8, 8));

addAndHandle(10, 20, (result) => { //this containes annonymous function that will be executed as callback in addAndHandle
    console.log(result);
})